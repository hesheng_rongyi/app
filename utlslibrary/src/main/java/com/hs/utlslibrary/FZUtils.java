package com.hs.utlslibrary;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/2/25  下午3:53.
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/2/25        ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class FZUtils {
    /**
     * 得到设备屏幕的宽度高度
     *
     * @param context 上下文
     * @return int
     */
    public static Rect getScreen(Context context) {
        Rect rect = new Rect();
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        rect.set(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        return rect;
    }

    /**
     * 得到设备的密度
     *
     * @param context 上下文
     * @return float
     */
    public static float getScreenDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    /**
     * 获取屏幕分辨率的宽高
     *
     * @param context
     * @return
     */
    public static Rect getResolutionScreen(Context context) {
        Rect rect = new Rect();
        Point screenPoint = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            ((Activity) context).getWindowManager().getDefaultDisplay().getRealSize(screenPoint);
        } else {
            ((Activity) context).getWindowManager().getDefaultDisplay().getSize(screenPoint);
        }
        rect.set(0, 0, screenPoint.x, screenPoint.y);
        return rect;
    }

    /**
     * 把dp转换为px
     *
     * @param context 上下文
     * @param dp      dp值
     * @return int
     */
    public static int dip2px(Context context, float dp) {
        return (int) (dp * getScreenDensity(context) + 0.5);
    }

    /**
     * 把sp转换为px
     *
     * @param context 上下文
     * @param sp      sp
     * @return int
     */
    public static int sp2px(Context context, float sp) {
        return (int) (sp * getScreenDensity(context));
    }

    /**
     * 将px值转换为sp值，保证文字大小不变
     *
     * @param pxValue
     * @return
     */
    public static int px2sp(Context context, float pxValue) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * 将px值转换为dip或dp值，保证尺寸大小不变
     *
     * @param pxValue
     * @return
     */
    public static int px2dip(Context context, float pxValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 跳转至拨号界面
     *
     * @param context     上下文
     * @param phoneNumber 电话号码
     */
    public static void callDial(Context context, String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber)));
        }
    }

    /**
     * 拨打电话
     *
     * @param context     上下文
     * @param phoneNumber 电话号码
     */
    @RequiresPermission("android.permission.CALL_PHONE")
    public static void call(Context context, String phoneNumber) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        callDial(context, phoneNumber);
    }

    /**
     * 跳转Activity（intent不需要携带数据）
     *
     * @param context   上下文
     * @param className 下一个页面类名
     */
    public static void intoNextActivity(Context context, Class<?> className) {
        if (className != null) {
            Intent intent = new Intent(context, className);
            context.startActivity(intent);
        }
    }

    /**
     * 获取application中指定的meta-data
     *
     * @param context 上下文
     * @param key     需要取值的KEY
     * @return 如果没有获取成功(没有对应值或者异常)，则返回值为空
     */
    public static String getAppMetaData(Context context, String key) {
        if (context == null || TextUtils.isEmpty(key)) {
            return null;
        }
        String resultData = null;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                if (applicationInfo != null) {
                    if (applicationInfo.metaData != null) {
                        resultData = applicationInfo.metaData.getString(key);
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return resultData;
    }

    /**
     * 设置View 是否显示
     *
     * @param view   需要设置的View对象
     * @param isGone 是否隐藏
     * @param <V>    V
     * @return V 当前View
     */
    public static <V extends View> V setGone(V view, boolean isGone) {
        if (view != null) {
            if (isGone) {
                if (View.GONE != view.getVisibility()) {
                    view.setVisibility(View.GONE);
                }
            } else {
                if (View.VISIBLE != view.getVisibility()) {
                    view.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    /**
     * 设置View 是否显示
     *
     * @param view   需要设置的View对象
     * @param isGone 是否隐藏
     * @param <V>    V
     * @return V 当前View
     */
    public static <V extends View> V setInvisible(V view, boolean isGone) {
        if (view != null) {
            if (isGone) {
                if (View.INVISIBLE != view.getVisibility()) {
                    view.setVisibility(View.INVISIBLE);
                }
            } else {
                if (View.VISIBLE != view.getVisibility()) {
                    view.setVisibility(View.VISIBLE);
                }
            }
        }
        return view;
    }

    /**
     * 多个view隐藏或显示
     *
     * @param gone  true 隐藏；false 显示
     * @param views 多个view对象
     */
    public static void setViewsInvisible(boolean gone, View... views) {
        for (View view : views) {
            setInvisible(view, gone);
        }
    }

    /**
     * 多个view隐藏或显示
     *
     * @param gone  true 隐藏；false 显示
     * @param views 多个view对象
     */
    public static void setViewsGone(boolean gone, View... views) {
        for (View view : views) {
            setGone(view, gone);
        }
    }

    /**
     * 判断APP是否处于前台
     *
     * @param context 上下文
     * @return true 处于前台 false 处于后台
     */
    public static boolean isApplicationBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        @SuppressWarnings("deprecation")
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 安装APK文件
     *
     * @param context 上下文
     * @param file    需要安装的APK文件
     */
    public static void installApk(Context context, File file) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setType("application/vnd.android.package-archive");
        intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * 当前设备是否是手机
     *
     * @param context 上下文
     * @return true 设备为手机
     */
    public static boolean isPhone(Context context) {
        TelephonyManager telephony = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        return telephony.getPhoneType() != TelephonyManager.PHONE_TYPE_NONE;
    }

    /**
     * 获取设备的 IMEI
     *
     * @param context 上下文
     * @return String
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public static String getDeviceIMEI(Context context) {
        String deviceId;
        if (isPhone(context)) {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephony.getDeviceId();
        } else {
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return deviceId;
    }

    /**
     * 获取设备的MAC地址
     *
     * @param context 上下文
     * @return String
     */
    public static String getMacAddress(Context context) {
        String macAddress;
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = wifi.getConnectionInfo();
        macAddress = info.getMacAddress();
        if (null == macAddress) {
            return "";
        }
        macAddress = macAddress.replace(":", "");
        return macAddress;
    }

    /**
     * 获取设备的mac地址（无网络也可以获取）
     *
     * @return
     */
    public static String getMacAddr() {
        String macSerial = null;
        String str = "";
        try {
            Process pp = Runtime.getRuntime().exec(
                    "cat /sys/class/net/wlan0/address ");
            InputStreamReader ir = new InputStreamReader(pp.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);


            for (; null != str; ) {
                str = input.readLine();
                if (str != null) {
                    macSerial = str.trim();// 去空格
                    break;
                }
            }
        } catch (IOException ex) {
            // 赋予默认值
            ex.printStackTrace();
        }
        return macSerial;
    }

    /**
     * 获取APP的版本号名称
     *
     * @param context 上下文
     * @return String
     */
    public static String getAppVersion(Context context) {
        String version = "1.0.0";
        try {
            version = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * 获取APP的版本号
     *
     * @param context 上下文
     * @return int
     */
    public static int getVersionCode(Context context) {
        int versionCode = 0;
        try {
            versionCode = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;  // 获取软件版本号
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    /**
     * 获取设备信息
     *
     * @param context 上下文
     * @return Properties
     */
    public static Properties collectDeviceInfo(Context context) {
        Properties mDeviceCrashInfo = new Properties();
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_ACTIVITIES);
            if (pi != null) {
                mDeviceCrashInfo.put("VERSION_NAME", pi.versionName == null ? "not set" : pi.versionName);
                mDeviceCrashInfo.put("VERSION_CODE", pi.versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("FZUtils", "Error while collect package info", e);
        }
        Field[] fields = Build.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                mDeviceCrashInfo.put(field.getName(), field.get(null));
            } catch (Exception e) {
                Log.e("FZUtils", "Error while collect crash info", e);
            }
        }

        return mDeviceCrashInfo;
    }

    /**
     * 获取设备信息的的字符串内容
     *
     * @param context 上下文
     * @return String
     */
    public static String collectDeviceInfoStr(Context context) {
        Properties prop = collectDeviceInfo(context);
        Set deviceInfos = prop.keySet();
        StringBuilder deviceInfoStr = new StringBuilder("{\n");
        for (Object item : deviceInfos) {
            deviceInfoStr.append("\t\t\t").append(item).append(":").append(prop.get(item)).append(", \n");
        }
        deviceInfoStr.append("}");
        return deviceInfoStr.toString();
    }

    /**
     * 隐藏软键盘
     *
     * @param activity Activity
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public static void hideSoftInput(Activity activity) {
        View view = activity.getWindow().peekDecorView();
        if (view != null) {
            InputMethodManager inputManger = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManger.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    /**
     * 隐藏软键盘
     *
     * @param context 上下文
     * @param edit    编辑框
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public static void hideSoftInput(Context context, EditText edit) {
        edit.clearFocus();
        InputMethodManager inputManger = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManger.hideSoftInputFromWindow(edit.getWindowToken(), 0);
    }

    /**
     * 显示软键盘
     *
     * @param context 上下文
     * @param edit    编辑框
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public static void showSoftInput(Context context, EditText edit) {
        edit.setFocusable(true);
        edit.setFocusableInTouchMode(true);
        edit.requestFocus();
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(edit, 0);
    }

    /**
     * 动态显示或者是隐藏软键盘
     *
     * @param context 上下文
     * @param edit    编辑框
     */
    @TargetApi(Build.VERSION_CODES.CUPCAKE)
    public static void toggleSoftInput(Context context, EditText edit) {
        edit.setFocusable(true);
        edit.setFocusableInTouchMode(true);
        edit.requestFocus();
        InputMethodManager inputManager = (InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static String getCurrentDateFormat(String dateFormat) {//"yyyy-MM-dd HH:mm:ss"
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);//可以方便地修改日期格式
        return simpleDateFormat.format(now);
    }

    /**
     * 获取当前时间是周几
     *
     * @param type 1：星期x  2:周x
     * @return
     */
    public static String getCurrentWeekOfDate(int type) {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        String[] weekdays2 = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        if (type == 1) {
            return weekDays[w];
        } else {
            return weekdays2[w];
        }
    }

    public static boolean isChinese(String str) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(regEx);
        Matcher matcher = pat.matcher(str);
        boolean flg = false;
        if (matcher.find())
            flg = true;

        return flg;
    }

    public static String timeIsAmOrPm() {
        int index = new GregorianCalendar().get(GregorianCalendar.AM_PM);
        return (0 == index) ? "上午" : "下午";
    }

    public static boolean isEmpty(String str) {
        return str == null || str.equals("null") || str.trim().equals("");
    }

    public static boolean notEmpty(String str) {
        return !isEmpty(str);
    }

    /**
     * 获取 EditText 的内容
     *
     * @param edt EditText 控件
     * @return String内容
     */
    public static String getEditTextContent(EditText edt) {
        return edt == null ? "" : edt.getText().toString().trim();
    }

    /**
     * 是否 EditText 内容为空
     *
     * @param edt EditText控件
     * @return true 内容为空
     */
    public static boolean isEditTextEmpty(EditText edt) {
        return isEmpty(getEditTextContent(edt));
    }

    /**
     * 获取TextView 的内容
     *
     * @param tv TextView控件
     * @return String内容
     */
    public static String getTextViewContent(TextView tv) {
        return tv == null ? "" : tv.getText().toString().trim();
    }

    /**
     * 判断汉字
     *
     * @param content 内容
     * @return true 是汉字
     */
    public static boolean hasChinese(String content) {
        String regEx = "[\\u4e00-\\u9fa5]+$";   //"^[\\u2E80-\\uFE4F]+$";   //"[\\u4e00-\\u9fa5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(content);
        return m.find();
    }

    /**
     * @param str: 需要验证的字符串
     * @return 含有表情返回true
     */
    public static boolean checkExpression(String str) {
        //匹配非表情符号的正则表达式
        String reg = "^([a-z]|[A-Z]|[0-9]|[\u2E80-\u9FFF]|[\\u4e00-\\u9fa5]|[`~!@#$%^&*()+=|{}':;',\\\\[\\\\]._<>/?~！@#￥%……&*（）\\-\\{\\}\\[\\]——+|{}【】‘；：”“’。，、？])+|@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?|[wap.]{4}|[www.]{4}|[blog.]{5}|[bbs.]{4}|[.com]{4}|[.cn]{3}|[.net]{4}|[.org]{4}|[http://]{7}|[ftp://]{6}$";
        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(str);
        return !matcher.matches();
    }

    /**
     * 检查手机号码
     *
     * @param phone 手机号码
     * @return true:正确的手机号码
     */
    public static boolean isPhoneNumber(String phone) {
        return notEmpty(phone) && phone.length() == 11;
    }

    /**
     * 设置 textView 控件的值 值为空时隐藏VIEW
     *
     * @param text   需要设置的值
     * @param target textView
     */
    public static void setTextEmptyGone(String text, TextView target) {
        if (isEmpty(text)) {
            FZUtils.setGone(target, true);
        } else {
            target.setText(text);
            FZUtils.setGone(target, false);
        }
    }

    /**
     * 设置 textView 控件的值 值为空时隐藏VIEW
     *
     * @param text   需要设置的值
     * @param target textView
     */
    public static void setTextEmptyInvisible(String text, TextView target) {
        if (isEmpty(text)) {
            FZUtils.setInvisible(target, true);
        } else {
            target.setText(text);
            FZUtils.setInvisible(target, false);
        }
    }

    /**
     * 字符串数组转字符串
     *
     * @param strings 需要转换的字符串数组
     * @return String
     */
    public static StringBuilder formatArrayStrings(String[] strings, int formatSize, String format) {
        StringBuilder buffer = new StringBuilder();
        if (strings != null && strings.length > 0) {
            int size = strings.length > formatSize ? formatSize : strings.length;//只取前两个标签
            for (int i = 0; i < size; i++) {
                buffer.append(strings[i]).append(format);
            }
        }
        return buffer;
    }

    /**
     * http://判断
     *
     * @param content 内容
     * @return boolean true:是网址 false:不是网址信息
     */
    public static boolean isWebsite(String content) {
        if (notEmpty(content)) {
            int index = content.indexOf("http://");
            return index == 0;
        } else {
            return false;
        }
    }
}
