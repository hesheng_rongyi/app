package com.hs.utlslibrary;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/2/25  下午1:53.
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/2/25        ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class FZFileHelper {
    /**
     * 读取文件内容 只适合读取小文件
     *
     * @param context  上下文
     * @param filePath 文件路径
     * @return 文件字符串
     */
    public static String readFileToString(Context context, String filePath) {
        return readFileToString(context, filePath, false);
    }

    /**
     * 读取文件内容 只适合读取小文件
     *
     * @param context  上下文
     * @param filePath 文件路径
     * @param isAssets 是否Assets目录文件
     * @return 文件字符串
     */
    public static String readFileToString(Context context, String filePath, boolean isAssets) {
        String res = "";
        InputStream in = null;
        FileInputStream inFile = null;
        int length;
        try {
            if (isAssets) {
                in = context.getResources().getAssets().open(filePath);
                length = in.available();
            } else {
                inFile = new FileInputStream(filePath);
                length = inFile.available();
            }
            byte[] buffer = new byte[length];
            int len;
            if (inFile != null) {
                len = inFile.read(buffer);
                inFile.close();
            } else {
                len = in.read(buffer);
                in.close();
            }
            if (len > 0) {
                final byte[] bom = new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};
                int start = 0;
                if (buffer[0] == bom[0] && buffer[1] == bom[1] && buffer[2] == bom[2]) {
                    start = 3;
                }
                res = new String(buffer, start, len, "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 读取文件内容 只适合读取小文件
     *
     * @param file 文件对象
     * @return 文件字符串
     */
    public static String readFileToString(File file) {
        String res = "";
        FileInputStream inFile;
        int length;
        try {
            inFile = new FileInputStream(file);
            length = inFile.available();
            byte[] buffer = new byte[length];
            int len;
            len = inFile.read(buffer);
            inFile.close();
            if (len > 0) {
                res = new String(buffer, 0, len, "UTF-8");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 将字符串写入文件 适合写小文件
     *
     * @param filePath 文件全路径
     * @param message  需要写入文件的字符串
     */
    public static void writeInfoToFile(String filePath, String message) {
        writeInfoToFile(filePath, message, false);
    }

    /**
     * 将字符串写入文件 适合写小文件
     *
     * @param filePath 文件全路径
     * @param message  需要写入文件的字符串
     * @param isAppend 是否追加
     */
    public static void writeInfoToFile(String filePath, String message, boolean isAppend) {
        try {
            FileOutputStream out = new FileOutputStream(filePath, isAppend);
            byte[] bytes = message.getBytes();
            out.write(bytes);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 将字符串写入文件 适合写小文件
     *
     * @param file    文件句柄
     * @param message 需要写入文件的字符串
     * @param append  是否追加
     */
    public static void writeInfoToFile(File file, String message, boolean append) {
        try {
            FileOutputStream out = new FileOutputStream(file, append);
            byte[] bytes = message.getBytes();
            out.write(bytes);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 文件是否存在
     *
     * @param filePath 文件全路径
     * @return true:存在 false:不存在
     */
    public static boolean isFileExit(String filePath) {
        return filePath != null && !filePath.isEmpty() && new File(filePath).exists();
    }

    /**
     * 检查目录是否存在，如果不存在则创建目录
     *
     * @param folder 目录路径
     * @return true: 目录文件存在 false: 目录不存在
     */
    public static boolean isFolderExists(String folder) {
        File file = new File(folder);
        return file.exists() || file.mkdirs();
    }

    /**
     * 删除文件接口
     *
     * @param filePath 需要删除的文件路径
     * @return true:成功 false:失败
     */
    public static boolean deleteFile(String filePath) {
        return isFileExit(filePath) && deleteFile(new File(filePath));
    }

    /**
     * 删除文件接口
     *
     * @param file 需要删除的文件
     * @return true:成功 false:失败
     */
    public static boolean deleteFile(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                return file.delete();
            } else if (file.isDirectory()) {
                return deleteDirectoryFile(file, true);
            }
        }
        return false;
    }

    /**
     * 删除文件夹及子文件接口
     *
     * @param file 需要删除的文件
     * @return true:成功 false:失败
     */
    public static boolean deleteDirectoryFile(File file, boolean isDeleteDirectory) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File f : files) {
                    deleteFile(f);
                }
                return !isDeleteDirectory || file.delete();
            }
        }
        return false;
    }


    public static void renameFile(String file, String toFile) {
        File toBeRenamed = new File(file);
        //检查要重命名的文件是否存在，是否是文件
        if (!toBeRenamed.exists() || toBeRenamed.isDirectory()) {
            System.out.println("File does not exist: " + file);
            return;
        }
        File newFile = new File(toFile);
        //修改文件名
        if (toBeRenamed.renameTo(newFile)) {
            System.out.println("File has been renamed.");
        } else {
            System.out.println("Error renmaing file");
        }
    }

    /**
     * 拷贝文件夹内容到另一个文件夹下
     *
     * @param srcFolder  需要拷贝的文件夹目录
     * @param destFolder 目的地文件夹目录
     * @return 是否拷贝成功 -1：拷贝失败 0：拷贝成功
     */
    public static int copyFolder(String srcFolder, String destFolder) {
        //要复制的文件目录
        File[] currentFiles;
        File root = new File(srcFolder);
        //如同判断SD卡是否存在或者文件是否存在
        //如果不存在则 return出去
        if (!root.exists() || !root.canRead() || TextUtils.isEmpty(destFolder)) {
            return -1;
        }

        //如果存在则获取当前目录下的全部文件 填充数组
        currentFiles = root.listFiles();

        //目标目录
        File targetDir = new File(destFolder);
        //创建目录
        if (!targetDir.exists()) {
            targetDir.mkdirs();
        }
        //遍历要复制该目录下的全部文件
        for (File currentFile : currentFiles) {
            if (currentFile.isDirectory()) {
                copyFolder(currentFile.getPath() + "/", destFolder + currentFile.getName() + "/");
            } else {
                copyFile(currentFile.getPath(), destFolder + currentFile.getName());
            }
        }
        return 0;
    }

    /**
     * 拷贝文件
     *
     * @param fromFile 需要拷贝的文件路径
     * @param toFile   目的地文件路径
     * @return 是否拷贝成功 -1：拷贝失败 0：拷贝成功
     */
    public static int copyFile(String fromFile, String toFile) {
        InputStream fosFrom = null;
        OutputStream fosTo = null;
        try {
            fosFrom = new FileInputStream(fromFile);
            fosTo = new FileOutputStream(toFile);
            byte bt[] = new byte[1024];
            int c;
            while ((c = fosFrom.read(bt)) > 0) {
                fosTo.write(bt, 0, c);
            }
            fosTo.flush();
            return 0;
        } catch (IOException ex) {
            return -1;
        } finally {
            if (fosFrom != null) {
                try {
                    fosFrom.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fosTo != null) {
                try {
                    fosTo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    /**
     * 清除本应用内部缓存(/data/data/com.xxx.xxx/cache)
     *
     * @param context 上下文
     */
    public static void cleanInternalCache(Context context) {
        deleteDirectoryFile(context.getCacheDir(), false);
    }

    /**
     * 清除本应用所有数据库(/data/data/com.xxx.xxx/databases)
     *
     * @param context 上下文
     */
    public static void cleanDatabases(Context context) {
        deleteDirectoryFile(new File("/data/data/"
                + context.getPackageName() + "/databases"), false);
    }

    /**
     * 清除本应用SharedPreference(/data/data/com.xxx.xxx/shared_prefs)
     *
     * @param context 上下文
     */
    public static void cleanSharedPreference(Context context) {
        deleteDirectoryFile(new File("/data/data/"
                + context.getPackageName() + "/shared_prefs"), false);
    }

    /**
     * 清除/data/data/com.xxx.xxx/files下的内容
     *
     * @param context 上下文
     */
    public static void cleanFiles(Context context) {
        deleteDirectoryFile(context.getFilesDir(), false);
    }

    /**
     * 清除外部cache下的内容(/mnt/sdcard/android/data/com.xxx.xxx/cache)
     *
     * @param context 上下文
     */
    public static void cleanExternalCache(Context context) {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            deleteDirectoryFile(context.getExternalCacheDir(), false);
        }
    }


    /**
     * 清除本应用所有的数据
     *
     * @param context 上下文
     */
    public static void cleanApplicationData(Context context) {
        cleanInternalCache(context);
        cleanExternalCache(context);
        cleanDatabases(context);
        cleanSharedPreference(context);
        cleanFiles(context);
    }

    /**
     * 是否有SD卡
     *
     * @return true 有SD卡
     */
    public static boolean haveSDCard() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }
}
