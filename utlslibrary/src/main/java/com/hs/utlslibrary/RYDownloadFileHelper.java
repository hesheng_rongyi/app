package com.hs.utlslibrary;

import android.text.TextUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      2017/8/25 下午4:23
 * Description: 文件同步下载工具类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 2017/8/25      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */

public class RYDownloadFileHelper {
    private static final String TAG = RYDownloadFileHelper.class.getSimpleName();

    /**
     * 下载文件数据
     *
     * @param sUrl 文件下载地址
     * @return 文件下载路径
     */
    public static String downloadFile(String sUrl, String downloadDirectory) {
        if (TextUtils.isEmpty(sUrl)) {
            return null;
        }
        int nameIndex = sUrl.lastIndexOf("/");
        String fileName = sUrl.substring(nameIndex + 1);
        return RYDownloadFileHelper.downloadFile(sUrl, downloadDirectory, fileName);
    }

    /**
     * 同步下载文件数据 需要在异步线程中执行相应逻辑
     *
     * @param sUrl          文件下载地址
     * @param localFilePath 文件路径
     * @param fileName      文件名
     * @return 文件下载路径
     */
    public static String downloadFile(String sUrl, String localFilePath, String fileName) {
        try {
            RYLogUtils.d(TAG, "--> 开始下载文件  --> url:" + sUrl + " --> 文件路径:" + localFilePath + "文件名:" + fileName);
            URL url = new URL(sUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            long fileSize = conn.getContentLength();

            File file = new File(localFilePath);
            if (!file.exists()) {
                file.mkdir();
            }

            File zipFile = new File(localFilePath + fileName);
            long downSize = 0;
            if (zipFile.exists()) {
                downSize = zipFile.length();
                if (downSize == fileSize) {
                    RYLogUtils.i(TAG, "--> 同名文件已存在！！！  --> 文件路径:" + zipFile.getAbsolutePath());
                    conn.disconnect();
                    return zipFile.getAbsolutePath();
                }
            }

            InputStream is = conn.getInputStream();
            long at = downSize;
            while (at > 0) {//循环跳过正确的文件长度
                long amt = is.skip(at);
                if (amt == -1) {
                    //文件跳过出错需要删除文件重头开始写文件
                    zipFile.delete();
                    break;
                }
                at -= amt;
            }
            if (!zipFile.exists()) {
                zipFile = new File(localFilePath + fileName);
            }
            FileOutputStream fos = new FileOutputStream(zipFile, true);
            byte buf[] = new byte[1024];
            int numRead;
            while ((numRead = is.read(buf)) != -1) {
                if (numRead <= 0) {
                    break;
                }
                fos.write(buf, 0, numRead);
            }

            RYLogUtils.d(TAG, "--> 文件下载完成！！！  --> url = " + sUrl);
            fos.close();
            is.close();
            conn.disconnect();
            return zipFile.getAbsolutePath();
        } catch (Exception e) {
            e.printStackTrace();
            RYLogUtils.e(TAG, "--> 文件下载出错！！！  --> e = " + e.getMessage());
            return null;
        }
    }
}
