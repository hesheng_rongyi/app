package com.hs.utlslibrary;


import com.hs.utlslibrary.xlog.Log;
import com.hs.utlslibrary.xlog.Xlog;

import com.orhanobut.logger.Logger;


/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/1/26  下午2:33.
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/1/26        ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class RYLogUtils {
    private static boolean DEBUG_D = true;
    private static boolean DEBUG_I = true;
    private static boolean DEBUG_W = true;
    private static boolean DEBUG_E = true;

    /**
     * 初始化腾讯log文件
     *
     * @param cachePath 缓存目录
     * @param logPath   日志目录
     */
    public static void init(String cachePath, String logPath) {
        if (BuildConfig.DEBUG) {
            Xlog.appenderOpen(Xlog.LEVEL_INFO, Xlog.AppednerModeAsync, cachePath, logPath, "ry", "");
            Xlog.setConsoleLogOpen(true);
        } else {
            Xlog.appenderOpen(Xlog.LEVEL_INFO, Xlog.AppednerModeAsync, cachePath, logPath, "ry", "");
            Xlog.setConsoleLogOpen(true);
        }
        Log.setLogImp(new Xlog());
    }

    /**
     * 设置LOG输出级别
     *
     * @param level 0,1,2,3,4  0-->全部输出
     */
    public static void setLogLevel(int level) {
        DEBUG_D = level >= 1;
        DEBUG_I = level >= 2;
        DEBUG_W = level >= 3;
        DEBUG_E = level >= 4;
    }

    public static void d(String tag, String msg) {
        if (DEBUG_D) {
            Logger.t(tag).d(msg);
//            Log.d(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (DEBUG_I) {
            Logger.t(tag).i(msg);
//            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (DEBUG_W) {
            Logger.t(tag).w(msg);
//            Log.w(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (DEBUG_E) {
            Logger.t(tag).e(msg);
//            Log.e(tag, msg);
        }
    }

    public static void json(String tag, String msg) {
        if (DEBUG_D) {
            Logger.t(tag).json(msg);
//            Log.d(tag, msg);
        }
    }
}
