package com.hs.baseapp.utls;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/8/2 下午3:39
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/8/2      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class FZDialogHelper {
    public static void showErrorDialog(Context context, String errorMsg, String okMsg) {
        new AlertDialog.Builder(context)
                .setMessage(errorMsg)
                .setPositiveButton(okMsg, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    public static void showErrorDialog(Context context, String errorMsg, int okResId) {
        new AlertDialog.Builder(context)
                .setMessage(errorMsg)
                .setPositiveButton(okResId, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
}
