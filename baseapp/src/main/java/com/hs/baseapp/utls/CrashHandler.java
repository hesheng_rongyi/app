package com.hs.baseapp.utls;


import com.hs.utlslibrary.RYLogUtils;

/**
 * Author:      hs
 * Version      V1.0
 * Date:        2017/12/8
 * Description:  捕获异常
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 2017/5/25         hs            1.0                    1.0
 * Why & What is modified:
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    public CrashHandler() {
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        RYLogUtils.d("CrashHandler", e.getMessage());
        killApp();
    }

    private void killApp() {
        android.os.Process.killProcess(android.os.Process.myPid());//再此之前可以做些退出等操作
    }
}
