package com.hs.baseapp.adpater;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Author:      hs
 * Version      V1.0
 * Date:        2017/8/7
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 2017/5/25         hs            1.0                    1.0
 * Why & What is modified:
 */

public class BasePageAdapter<T extends View> extends PagerAdapter {
    private List<T> mList = new ArrayList<>();

    public BasePageAdapter() {

    }

    public BasePageAdapter(List<T> list) {
        setList(list);
    }

    public void clean() {
        mList.clear();
    }

    public void setList(List<T> list) {
        this.mList.clear();
        this.mList.addAll(list);
    }

    public View getView(int position) {
        return position >= 0 && position < mList.size() ? mList.get(position) : null;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mList.get(position));
        return mList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mList.get(position));
    }
}
