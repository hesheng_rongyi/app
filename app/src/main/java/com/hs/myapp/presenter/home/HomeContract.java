package com.hs.myapp.presenter.home;

import android.content.Context;

import com.hs.basenetworklibrary.mvp.IBaseModel;
import com.hs.basenetworklibrary.mvp.IBasePresenter;
import com.hs.basenetworklibrary.mvp.IBaseView;
import com.hs.myapp.bean.HomeData;
import com.hs.myapp.param.IdParam;

import io.reactivex.Observer;


public interface HomeContract {
    interface Model extends IBaseModel {
        void getHomeData(Observer<HomeData> observer, IdParam param);
    }

    interface View extends IBaseView {
        void showGetHomeDataSuccess(HomeData homeData); //显示获取首页数据成功

        void showGetHomeDataFail(String msg); //显示获取首页数据失败

        IdParam getHomeParam(); //获取首页请求参数
    }

    interface Presenter extends IBasePresenter {
        void getHomeData(Context context);
    }
}
