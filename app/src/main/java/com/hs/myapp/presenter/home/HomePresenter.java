package com.hs.myapp.presenter.home;

import android.content.Context;
import android.support.annotation.NonNull;

import com.hs.basenetworklibrary.mvp.BasePresenter;
import com.hs.basenetworklibrary.progress.ProgressSubscriber;
import com.hs.basenetworklibrary.progress.SubscriberOnNextListener;
import com.hs.myapp.api.ApiMethods;
import com.hs.myapp.bean.HomeData;

public class HomePresenter extends BasePresenter implements HomeContract.Presenter {
    @NonNull
    private final HomeContract.View mView;
    private final HomeModel mHomeModel;

    public HomePresenter(Context context, @NonNull HomeContract.View view) {
        super();
        mView = view;
        mHomeModel = new HomeModel(context, ApiMethods.instance());
    }

    @Override
    public void getHomeData(Context context) {
        mSubscriptions.clear();
        SubscriberOnNextListener<HomeData> subscriberOnNextListener = new SubscriberOnNextListener<HomeData>() {
            @Override
            public void onNext(HomeData homeData) {
                mView.showGetHomeDataSuccess(homeData);
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                mView.showGetHomeDataFail(e.getMessage());
            }
        };
        ProgressSubscriber<HomeData> subscriber = new ProgressSubscriber<HomeData>(subscriberOnNextListener, context);
        mHomeModel.getHomeData(subscriber, mView.getHomeParam());
        mSubscriptions.add(subscriber);

    }
}
