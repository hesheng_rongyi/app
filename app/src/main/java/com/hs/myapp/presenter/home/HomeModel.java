package com.hs.myapp.presenter.home;

import android.content.Context;

import com.hs.basenetworklibrary.model.RYBaseNetworkModel;
import com.hs.myapp.api.ApiMethods;
import com.hs.myapp.bean.HomeData;
import com.hs.myapp.param.IdParam;

import io.reactivex.Observer;


public class HomeModel extends RYBaseNetworkModel<ApiMethods> implements HomeContract.Model {
    public HomeModel(Context context, ApiMethods httpApiMethods) {
        super(context, httpApiMethods);
    }

    @Override
    public void getHomeData(Observer<HomeData> observer, IdParam param) {
        httpApiMethods.getHomeData(observer, param);
    }

}
