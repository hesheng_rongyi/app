package com.hs.myapp.api;


import com.hs.basenetworklibrary.api.RYRedirectResponseTransformer;
import com.hs.basenetworklibrary.exception.RYApiException;

public class RedirectResponseTransformer<T> extends RYRedirectResponseTransformer<T> {
    @Override
    public RYApiException getApiException(String msg) {
        return new RYApiException(msg);
    }

    @Override
    public RYApiException getApiException(String msg, int code) {
        return new RYApiException(msg, code);
    }
}
