package com.hs.myapp.api;

import com.hs.basenetworklibrary.bean.HttpResult;
import com.hs.myapp.bean.HomeData;
import com.hs.myapp.param.IdParam;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface ApiService {

    /**
     * @param json {@link IdParam}
     * @return
     */
    @POST("easy-smart/eshelfCaseLayer/queryHomePage")
    Observable<HttpResult<HomeData>> getHomeData(@Body RequestBody json);
}
