package com.hs.myapp.api;

import android.text.TextUtils;

import com.hs.baseapp.app.FZBaseApplication;
import com.hs.basenetworklibrary.BuildConfig;
import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;
import com.hs.myapp.bean.HomeData;
import com.hs.myapp.param.IdParam;
import com.hs.utlslibrary.FZUtils;
import com.hs.utlslibrary.RYLogUtils;
import com.hs.utlslibrary.RYNetworkInfoHelper;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observer;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiMethods extends RYBaseHttpApiMethods {
    private static final long DEFAULT_TIMEOUT = 61;
    private static final long HTTP_RESPONSE_DISK_CACHE_MAX_SIZE = 10 * 1024 * 1024;//10MB
    static volatile ApiMethods singleton = null;
    private ApiService appApiService;


    //构造方法私有
    private ApiMethods(ApiService appApiService) {
        super(FZBaseApplication.getContext());
        this.appApiService = appApiService;
    }

    //在访问HttpMethods时创建单例
    public static ApiMethods instance() {
        if (singleton == null) {
            synchronized (ApiMethods.class) {
                if (singleton == null) {
                    singleton = new Builder(createApiService()).build();
                }
            }
        }
        return singleton;
    }

    private static class Builder {
        private ApiService appApiService;

        public Builder(ApiService appApiService) {
            if (appApiService == null) {
                throw new IllegalArgumentException("AppApiService must not be null.");
            }
            this.appApiService = appApiService;
        }

        public ApiMethods build() {
            return new ApiMethods(appApiService);
        }
    }

    /**
     * 接口全局埋点字段信息
     * ua 设备 终端设备
     * appVersion APP版本号
     * osVersion 系统版本号
     * netWork 网络类型
     */
    private static ApiService createApiService() {
        Interceptor requestInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                String mac = FZUtils.getMacAddr();
                Request request = chain.request()
                        .newBuilder()
                        .addHeader("ua", "Android")
                        .addHeader("appVersion", BuildConfig.VERSION_NAME)
                        .addHeader("osVersion", android.os.Build.VERSION.RELEASE)
                        .addHeader("deviceType", android.os.Build.MODEL)
                        .addHeader("netWork", RYNetworkInfoHelper.getCurrentNetType(FZBaseApplication.getContext()))
                        .addHeader("mac", TextUtils.isEmpty(mac) ? "UN_KNOWN" : mac)
                        .build();
                RYLogUtils.d("url", request.url().toString());
                return chain.proceed(request);
            }
        };
        //云端响应头拦截器，用来配置缓存策略
        Interceptor rewriteCacheControlInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                if (!RYNetworkInfoHelper.isNetworkAvailable(FZBaseApplication.getContext())) {
                    request = request.newBuilder()
                            .cacheControl(CacheControl.FORCE_CACHE)
                            .build();
                }
                Response originalResponse = chain.proceed(request);
                if (RYNetworkInfoHelper.isNetworkAvailable(FZBaseApplication.getContext())) {
                    //有网的时候读接口上的@Headers里的配置，你可以在这里进行统一的设置
                    String cacheControl = request.cacheControl().toString();
                    return originalResponse.newBuilder()
                            .header("Cache-Control", cacheControl)
                            .removeHeader("Pragma")
                            .build();
                } else {
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, only-if-cached, max-stale=2419200")
                            .removeHeader("Pragma")
                            .build();
                }
            }
        };
        File baseDir = FZBaseApplication.getContext().getCacheDir();
        final File cacheDir = new File(baseDir, "HttpResponseCache");
        //手动创建一个OkHttpClient并设置超时时间
        OkHttpClient httpClientBuilder = new OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addNetworkInterceptor(rewriteCacheControlInterceptor)
                .addInterceptor(rewriteCacheControlInterceptor)
                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
                .cache(new Cache(cacheDir, HTTP_RESPONSE_DISK_CACHE_MAX_SIZE))
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClientBuilder)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(BuildConfig.API_HOST)
                .build();
        return retrofit.create(ApiService.class);
    }

    public void getHomeData(Observer<HomeData> observer, IdParam param) {
        toSubscribe(appApiService.getHomeData(createRequestBody(param.toJson()))
                        .compose(new RedirectResponseTransformer<HomeData>()),
                observer);
    }
}

