package com.hs.myapp.app;

import com.hs.baseapp.app.FZBaseApplication;

public class AppApplication extends FZBaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static AppApplication getAppApplication() {
        return (AppApplication) sContext;
    }
}
