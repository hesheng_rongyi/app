package com.hs.myapp.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.hs.baseapp.base.FZBaseActionBarActivity;
import com.hs.myapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends FZBaseActionBarActivity {
    @BindView(R.id.tv_text)
    TextView mTvText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mTvText.setText("");
    }
}
