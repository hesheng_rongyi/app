package com.hs.basenetworklibrary.model;

import android.content.Context;

import com.hs.basenetworklibrary.api.RYApiContact;
import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;

import io.reactivex.Observer;


/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/5/12 上午10:20
 * Description: 分页数据模型基础类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/5/12      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public abstract class RYBaseNetworkPageModel<T, H extends RYBaseHttpApiMethods> extends RYBaseNetworkModel<H> {
    protected int currentPage = RYApiContact.DEFAULT_CURRENT_PAGE;

    public RYBaseNetworkPageModel(Context context, H httpApiMethods) {
        super(context, httpApiMethods);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public boolean isRefresh() {
        return currentPage == RYApiContact.DEFAULT_CURRENT_PAGE;
    }

    protected abstract void refreshData(Observer<T> observer);//加载最新数据

    protected abstract void loadMoreData(Observer<T> observer);//加载更多数据

    protected abstract void loadNetworkData(Observer<T> observer);//加载网络数据
}
