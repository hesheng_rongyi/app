package com.hs.basenetworklibrary.model;

import android.content.Context;

import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;
import com.hs.utlslibrary.RYNetworkInfoHelper;


/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      2017/2/27 下午6:44
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 2017/2/27      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class RYBaseNetworkModel<H extends RYBaseHttpApiMethods> {
    protected H httpApiMethods;
    protected Context mContext;

    public RYBaseNetworkModel(Context context, H httpApiMethods) {
        this.httpApiMethods = httpApiMethods;
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    protected boolean isOnline() {//有网络加载网络数据
        return RYNetworkInfoHelper.isOnline(mContext);
    }
}
