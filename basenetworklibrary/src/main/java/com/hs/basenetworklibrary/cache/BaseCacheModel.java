package com.hs.basenetworklibrary.cache;


import android.content.Context;

import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;
import com.hs.basenetworklibrary.model.RYBaseNetworkModel;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/5/12 上午10:30
 * Description: 数据缓存模型基础类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/5/12      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public abstract class BaseCacheModel<T, D, H extends RYBaseHttpApiMethods> extends RYBaseNetworkModel<H> {
    protected RYBaseCacheData mCacheDatabase;

    public BaseCacheModel(Context context, H httpApiMethods) {
        super(context, httpApiMethods);
        mCacheDatabase=getCacheDatabase();
    }

    protected void getData(Observer<T> observer, D param) {
        if (isOnline()) {
            loadNetworkData(observer, param);
        } else {
            loadCacheData(observer, param);
        }
    }

    protected abstract RYBaseCacheData getCacheDatabase();

    protected void loadCacheData(Observer<T> observer, final D param) {
        Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<T> e) throws Exception {
                T data = loadCacheData(param);
                if (data != null) {
                    e.onNext(data);
                }
                e.onComplete();
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    protected abstract T loadCacheData(D param);//加载缓存数据

    protected abstract void loadNetworkData(Observer<T> observer, D param);//加载网络数据
}
