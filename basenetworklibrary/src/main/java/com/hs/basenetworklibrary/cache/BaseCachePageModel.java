package com.hs.basenetworklibrary.cache;

import android.content.Context;

import com.hs.basenetworklibrary.api.RYApiContact;
import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;

import io.reactivex.Observer;

/**
 * Author:    CaoKang
 * Version    V1.0
 * Date:      2017/11/16
 * Description:分页缓存Model
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 2017/11/16      CaoKang            1.0                    1.0
 * Why & What is modified:
 */

public abstract class BaseCachePageModel<T, D, H extends RYBaseHttpApiMethods> extends BaseCacheModel<T,D,H> {
    protected int currentPage = RYApiContact.DEFAULT_CURRENT_PAGE;

    public BaseCachePageModel(Context context, H httpApiMethods) {
        super(context, httpApiMethods);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public boolean isRefresh() {
        return currentPage == RYApiContact.DEFAULT_CURRENT_PAGE;
    }

    protected void getData(Observer<T> observer, D param, boolean isRefresh) {
        if (isOnline()) {
            if (isRefresh) {
                refreshData(observer, param);
            } else {
                loadMoreData(observer, param);
            }
        } else {
            if (isRefresh) {
                loadCacheData(observer, param);
            }
        }
    }

    protected abstract void refreshData(Observer<T> observer, D param);//加载最新数据

    protected abstract void loadMoreData(Observer<T> observer, D param);//加载更多数据

}
