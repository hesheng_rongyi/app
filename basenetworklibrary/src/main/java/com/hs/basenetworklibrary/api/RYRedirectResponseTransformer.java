package com.hs.basenetworklibrary.api;

import android.text.TextUtils;

import com.hs.basenetworklibrary.bean.HttpResult;
import com.hs.basenetworklibrary.exception.RYApiException;
import com.hs.utlslibrary.RYLogUtils;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import io.reactivex.Observable;
import io.reactivex.ObservableOperator;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/5/3 上午11:09
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/5/3      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public abstract class RYRedirectResponseTransformer<T> implements ObservableTransformer<HttpResult<T>, T> {
    //对应HTTP的状态码
    private static final int REQUEST_TIMEOUT = 408;
    private static final int GATEWAY_TIMEOUT = 504;

    @Override
    public ObservableSource<T> apply(@NonNull Observable<HttpResult<T>> httpResultObservable) {
        return httpResultObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .lift(new ObservableOperator<T, HttpResult<T>>() {
                    @Override
                    public Observer<? super HttpResult<T>> apply(final @NonNull Observer<? super T> observer) throws Exception {
                        return new Observer<HttpResult<T>>() {
                            @Override
                            public void onSubscribe(@NonNull Disposable d) {
                                observer.onSubscribe(d);
                            }

                            @Override
                            public void onNext(@NonNull HttpResult<T> httpResult) {
                                RYLogUtils.json("result", httpResult.toJson());
                                if (httpResult.isSuccess()) {
                                    if (httpResult.getData() != null) {
                                        observer.onNext(httpResult.getData());
                                    } else {
                                        observer.onComplete();
                                    }
                                } else {
                                    String msg = RYBaseHttpApiMethods.DEFAULT_ERROR_MSG;
                                    if (!TextUtils.isEmpty(httpResult.getMsg())) {
                                        msg = httpResult.getMsg();
                                    }
                                    observer.onError(getApiException(msg, httpResult.getErrorCode()));
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                Throwable throwable = e;
                                //获取最根源的异常
                                while (throwable.getCause() != null) {
                                    e = throwable;
                                    throwable = throwable.getCause();
                                }
                                if (e instanceof HttpException) {
                                    HttpException httpException = (HttpException) e;
                                    switch (httpException.code()) {
                                        case REQUEST_TIMEOUT:
                                        case GATEWAY_TIMEOUT: {
                                            observer.onError(getApiException(RYBaseHttpApiMethods.NETWORK_TIMEOUT_ERROR_MSG, RYApiException.TIME_OUT_ERROR_CODE));
                                        }
                                        break;
                                        default:
                                            observer.onError(getApiException(RYBaseHttpApiMethods.DEFAULT_ERROR_MSG));
                                            break;
                                    }
                                } else if (e instanceof ConnectException || e instanceof SocketTimeoutException) {
                                    observer.onError(getApiException(RYBaseHttpApiMethods.NETWORK_TIMEOUT_ERROR_MSG, RYApiException.TIME_OUT_ERROR_CODE));
                                } else {
                                    observer.onError(e);
                                }
                            }

                            @Override
                            public void onComplete() {
                                observer.onComplete();
                            }
                        };
                    }
                });
    }

    public abstract RYApiException getApiException(String msg);

    public abstract RYApiException getApiException(String msg, int code);
}
