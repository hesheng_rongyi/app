package com.hs.basenetworklibrary.api;


import android.content.Context;

import com.hs.basenetworklibrary.exception.RYApiException;
import com.hs.utlslibrary.RYLogUtils;
import com.hs.utlslibrary.RYNetworkInfoHelper;

import java.io.File;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/4/27 下午4:53
 * Description: APP api 请求工具类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/4/27      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class RYBaseHttpApiMethods {
    private static final String TAG = RYBaseHttpApiMethods.class.getSimpleName();
    public static final String DEFAULT_ERROR_MSG = "Whoops！网络不给力\n快找个信号满满的地方再刷新一下吧";
    public static final String NETWORK_ERROR_MSG = "没有网络连接,请打开你的网络连接";
    public static final String NETWORK_TIMEOUT_ERROR_MSG = "网络通信出现问题,请确认您的网络状况良好后重试";
    public static final String USER_LOGOUT_ERROR_MSG = "请重新登录";

    protected Context mContext;

    public RYBaseHttpApiMethods(Context context) {
        mContext = context.getApplicationContext();
    }

    public static String genRandomPwd() {
        final int maxNum = 36;
        int i;
        int count = 0;
        int pwdLength = 8;
        char[] str = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
                'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        StringBuilder pwd = new StringBuilder("");
        Random r = new Random();
        while (count < pwdLength) {
            i = Math.abs(r.nextInt(maxNum));
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }

    private static final ObservableTransformer ioTransformer = new ObservableTransformer() {
        @Override
        public ObservableSource apply(@NonNull Observable upstream) {
            return upstream.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());
        }
    };

    private static <T> ObservableTransformer<T, T> applyIoSchedulers() {
        return (ObservableTransformer<T, T>) ioTransformer;
    }

    protected void toSubscribe(Observable<?> observable, Observer subscriber) {
        toSubscribe(observable, subscriber, true);
    }

    private void toSubscribe(Observable<?> observable, Observer subscriber, boolean isRetry) {
        if (checkNetwork(subscriber)) {
            if (isRetry) {
                observable.compose(applyIoSchedulers())
                        .retryWhen(new RetryWithDelay(3, 3000))//总共重试3次，重试间隔3000毫秒
                        .subscribe(subscriber);
            } else {
                observable.compose(applyIoSchedulers())
                        .subscribe(subscriber);
            }
        }
    }

    /**
     * 检查是否有网络连接
     *
     * @param subscriber 订阅事件
     * @return true 有网络连接
     */
    private boolean checkNetwork(Observer subscriber) {
        if (!RYNetworkInfoHelper.isOnline(mContext)) {
            subscriber.onError(new RYApiException(NETWORK_ERROR_MSG));
            return false;
        }
        return true;
    }

    /**
     * 创建文件类型的 RequestBody
     *
     * @param path 文件全路经
     */
    protected MultipartBody.Part createFilePart(String path) {
        RYLogUtils.d(TAG, path);
        File file = new File(path);
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(RYApiContact.HTTP_FILE_TYPE), file);
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData("myfiles", file.getName(), requestFile);
    }

    /**
     * 创建文本类型的 RequestBody
     *
     * @param json 文本数据
     */
    protected RequestBody createRequestBody(String json) {
        RYLogUtils.d(TAG, json);
        return RequestBody.create(MediaType.parse(RYApiContact.HTTP_INPUT_TYPE), json);
    }

    /**
     * 创建文本流类型的 RequestBody
     *
     * @param json 文本数据
     */
    protected RequestBody createRequestBody(byte[] json) {
        return RequestBody.create(MediaType.parse(RYApiContact.HTTP_INPUT_TYPE), json);
    }
}
