package com.hs.basenetworklibrary.api;


import com.hs.basenetworklibrary.exception.RYApiException;
import com.hs.utlslibrary.RYLogUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;


/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/8/18 上午10:30
 * Description: 重试机制转换
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/8/18      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class RetryWithDelay implements Function<Observable<? extends Throwable>, Observable<?>> {
    private final int mMaxRetries;//重试次数
    private final int mRetryDelayMillis;//每次重试延时时间
    private int mRetryCount;//当前重试次数

    public RetryWithDelay(int maxRetries, int retryDelayMillis) {
        mMaxRetries = maxRetries;
        mRetryDelayMillis = retryDelayMillis;
    }

    @Override
    public Observable<?> apply(@NonNull Observable<? extends Throwable> observable) throws Exception {
        return observable.flatMap(new Function<Throwable, ObservableSource<?>>() {
            @Override
            public ObservableSource<?> apply(@NonNull Throwable throwable) throws Exception {
                Throwable e = throwable;
                while (e.getCause() != null) {
                    throwable = e;
                    e = e.getCause();
                }
                if (e instanceof RYApiException) {
                    RYApiException RYApiException = (RYApiException) e;
                    if (RYApiException.isTimeOutError()) {
                        if (++mRetryCount <= mMaxRetries) {
                            RYLogUtils.d("RetryWithDelay", "服务器超时错误,将在 " + mRetryDelayMillis + " 秒之后重试, 当前重试次数: " + mRetryCount);
                            return Observable.timer(mRetryDelayMillis, TimeUnit.MILLISECONDS);
                        }
                    }
                }
                return Observable.error(throwable);
            }
        });
    }
}
