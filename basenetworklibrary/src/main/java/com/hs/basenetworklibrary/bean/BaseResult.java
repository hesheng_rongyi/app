package com.hs.basenetworklibrary.bean;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/5/9 下午4:44
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/5/9      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class BaseResult<T> extends BaseBean {
    public T data;
    public Page page;

    public T getData() {
        return data;
    }
}
