package com.hs.basenetworklibrary.bean;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/4/29 下午6:36
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/4/29      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class HttpResult<T> extends BaseMeta {
    public BaseResult<T> result;

    public T getData() {
        return result != null ? result.getData() : null;
    }
}
