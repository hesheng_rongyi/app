package com.hs.basenetworklibrary.bean;

import android.text.TextUtils;

import com.hs.basenetworklibrary.api.RYApiContact;


/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/5/9 下午4:34
 * Description:
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/5/9      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class BaseMeta extends BaseBean {
    public Meta meta;

    public boolean isSuccess() {
        return meta != null && meta.errno == RYApiContact.STATUS_NET_SUCCESS;
    }

    public String getMsg() {
        return (meta == null || TextUtils.isEmpty(meta.msg)) ? "" : meta.msg;
    }

    public int getErrorCode() {
        return meta != null ? meta.errno : -1;
    }

    public boolean isLogout() {
        return meta != null && (meta.errno == RYApiContact.STATUS_NET_USER_LOGOUT || meta.errno == RYApiContact.STATUS_NET_USER_INVALID);
    }
}
