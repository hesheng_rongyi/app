package com.hs.basenetworklibrary.mvp;


import io.reactivex.disposables.CompositeDisposable;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/4/30 下午12:26
 * Description: Presenter 基础类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/4/30      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class BasePresenter implements IBasePresenter {
    protected CompositeDisposable mSubscriptions;
    protected final String TAG = getClass().getSimpleName();

    public BasePresenter() {
        mSubscriptions = new CompositeDisposable();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mSubscriptions.clear();
    }
}
