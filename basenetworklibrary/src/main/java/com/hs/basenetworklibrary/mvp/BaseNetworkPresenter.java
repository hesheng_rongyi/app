package com.hs.basenetworklibrary.mvp;


import com.hs.basenetworklibrary.api.RYBaseHttpApiMethods;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Author:    ZhuWenWu
 * Version    V1.0
 * Date:      16/4/30 下午12:26
 * Description: 网络请求 Presenter 基础类
 * Modification  History:
 * Date         	Author        		Version        	Description
 * -----------------------------------------------------------------------------------
 * 16/4/30      ZhuWenWu            1.0                    1.0
 * Why & What is modified:
 */
public class BaseNetworkPresenter<T extends RYBaseHttpApiMethods> implements IBasePresenter {
    protected CompositeDisposable mSubscriptions;
    protected final String TAG = getClass().getSimpleName();
    protected T mHttpApiMethods;

    public BaseNetworkPresenter(T httpApiMethods) {
        mHttpApiMethods = httpApiMethods;
        mSubscriptions = new CompositeDisposable();
    }

    @Override
    public void subscribe() {

    }

    @Override
    public void unSubscribe() {
        mSubscriptions.clear();
    }
}
